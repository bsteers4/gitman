# GitMan , simple git manager.


I have barely scratched the surface of git and it's capabilities but\
for now with my basic needs and my basic understanding i've wtitten a basic app,\
 in gambas basic. :^)

This app is very much beta as it's only just begun developement.\
Some buttons may not work yet.

## Features..
* Select a git clone folder and it is added to a pop-down list for quick access.
* Run various commands on folders/files like git status, git diff, etc
* Branches tab, Branches are listed and can be switched to by double clicking them.
* Modifications tab, Modifications are shown in a list, can be selected and added/removed/etc
* Built in custom TerminalView. Ctrl-V paste is enabled. and Ctrl-C will ask to copy text if any selected or send Ctrl-C to Terminal.
* Git status info Ie. **modified: myFile.txt** displayed in status panel will select filename on click and comands can be done on selected filename text.
* Selected (double clicked) filenames in TerminalView can also run comands on selected text
* Both GitHub and GitLab UserID's and passwords can be saved (sorry not securely encrypted) and sent to the terminal at the click of a button (the app detects if github or gitlab uri).
* Directory can be opened from the app.
* **push** and **pull**, also **pull upstream master** , more to be added to this.


Like i say it's a work in progress as is my knowledge of git.

But saying that it's quite functional and I'm finding it quite usefull so I'm sharing it with the world..

## Thanks to...
* Beno�t Minisini: Author of Gambas. Many thanks to Ben for all his work on gambas, his advice, his understanding and patience.
* Fabien Bodard: his advice on being able to add the gambas terminalview object in a way i can modify it without having to ask Ben to. :)
* All those who have helped on the forums.
* git and gitlab, I am only just discovering the power of all this.


Wishing well.\
## Bruce Steers.
